package kg.weather.base

import android.arch.lifecycle.ViewModel
import kg.weather.injection.component.DaggerViewModelInjector
import kg.weather.injection.component.ViewModelInjector
import kg.weather.injection.module.NetworkModule
import kg.weather.ui.main.viewModel.WeatherViewModel

abstract class BaseViewModel:ViewModel(){
    private val injector: ViewModelInjector = DaggerViewModelInjector
            .builder()
            .networkModule(NetworkModule)
            .build()

    init {
        inject()
    }

    /**
     * Injects the required dependencies
     */
    private fun inject() {
        when (this) {
            is WeatherViewModel -> injector.inject(this)
        }
    }
}