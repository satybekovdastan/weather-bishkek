package kg.weather.injection.component

import dagger.Component
import kg.weather.injection.module.NetworkModule
import kg.weather.ui.main.viewModel.WeatherViewModel
import javax.inject.Singleton

/**
 * Component providing inject() methods for presenters.
 */
@Singleton
@Component(modules = [(NetworkModule::class)])
interface ViewModelInjector {
    /**
     * Injects required dependencies into the specified WeatherViewModel.
     * @param weatherViewModel WeatherViewModel in which to inject the dependencies
     */
    fun inject(weatherViewModel: WeatherViewModel)

    /**
     * Injects required dependencies into the specified PostViewModel.
     * @param postViewModel PostViewModel in which to inject the dependencies
     */

    @Component.Builder
    interface Builder {
        fun build(): ViewModelInjector
        fun networkModule(networkModule: NetworkModule): Builder
    }
}