package kg.weather.injection

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.content.Context
import kg.weather.data.room.AppDatabase
import kg.weather.ui.main.viewModel.WeatherViewModel

class ViewModelFactory(private val context: Context): ViewModelProvider.Factory{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(WeatherViewModel::class.java)) {
            var db = AppDatabase.invoke(context)
            @Suppress("UNCHECKED_CAST")
            return WeatherViewModel(db.noteDao(), context) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")

    }
}