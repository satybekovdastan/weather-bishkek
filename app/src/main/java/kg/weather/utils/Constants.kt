package kg.weather.utils

import android.annotation.SuppressLint
import android.content.Context
import android.net.ConnectivityManager

/** The base URL of the API */
var ENDPOINT_URL = "https://api.openweathermap.org/"
var APPID = "6cf5353facde1994c30787e8b2559d6c"
var CITY = "Бишкек"
var UNITS = "metric"

fun isIntertnet(context: Context): Boolean {
    val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    @SuppressLint("MissingPermission") val networkInfo = cm.activeNetworkInfo
    return networkInfo != null && networkInfo.isConnectedOrConnecting
}
