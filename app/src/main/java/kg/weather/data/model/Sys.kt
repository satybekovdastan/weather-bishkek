package kg.weather.data.model

data class Sys(
    val country: String
)