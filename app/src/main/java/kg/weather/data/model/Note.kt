package kg.weather.data.model

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity
data class Note (
    @PrimaryKey
    var note: String
)