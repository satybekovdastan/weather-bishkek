package kg.weather.data.model

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity
data class ApiResponse (
    @PrimaryKey
    var id: Int,
    var timezone: Int,
    var name:String,
    var cod:Int,
    var main: Main,
    var sys: Sys,
    var weather: ArrayList<Weather>
)