package kg.weather.data.model

data class Main(
    val temp: Double,
    val temp_max: Double
)