package kg.weather.data.model

data class Weather(
    val id: Int,
    val main: String
)