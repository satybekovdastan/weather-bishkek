package kg.weather.data.network

import io.reactivex.Observable
import kg.weather.data.model.ApiResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * The interface which provides methods to get result of webservices
 */
interface PostApi {
    /**
     * Get the weather of the pots from the API
     */

    @GET("data/2.5/weather")
    fun getWeather(@Query("q") city: String, @Query("appid") appid: String,  @Query("units") units: String) : Observable<Response<ApiResponse>>

}