package kg.weather.data.room;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;
import kg.weather.data.model.Note;

@Dao
public interface NoteDao {

    @Query("SELECT * FROM note")
    Note getNote();

    @Insert
    void insert(Note note);

    @Update
    void update(Note note);

    @Query("UPDATE note SET note=:note WHERE note = :notePrimary")
    void updateNote(String notePrimary, String note);

    @Delete
    void delete(Note note);

}