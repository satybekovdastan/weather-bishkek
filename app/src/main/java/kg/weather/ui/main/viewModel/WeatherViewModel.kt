package kg.weather.ui.main.viewModel

import android.annotation.SuppressLint
import android.arch.lifecycle.MutableLiveData
import android.content.Context
import android.view.View
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kg.weather.R
import kg.weather.base.BaseViewModel
import kg.weather.data.model.ApiResponse
import kg.weather.data.model.Note
import kg.weather.data.network.PostApi
import kg.weather.data.room.NoteDao
import kg.weather.utils.APPID
import kg.weather.utils.CITY
import kg.weather.utils.UNITS
import kg.weather.utils.isIntertnet
import javax.inject.Inject

class WeatherViewModel(private val noteDao: NoteDao, @SuppressLint("StaticFieldLeak") private val context: Context) :
    BaseViewModel() {

    @Inject
    lateinit var postApi: PostApi

    val loadingVisibility: MutableLiveData<Int> = MutableLiveData()
    val responseData: MutableLiveData<ApiResponse> = MutableLiveData()
    val errorMessage: MutableLiveData<Int> = MutableLiveData()
    val note: MutableLiveData<Note> = MutableLiveData()
    val errorClickListener = View.OnClickListener { loadPosts() }
    private var subscription: CompositeDisposable = CompositeDisposable()

    init {
        loadPosts()
        loadNote()
    }

    override fun onCleared() {
        super.onCleared()
        subscription = CompositeDisposable()
    }

    private fun loadPosts() {
        if (isIntertnet(context)) {
            subscription.add(
                postApi.getWeather(CITY, APPID, UNITS)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe { onRetrievePostListStart() }
                    .doOnTerminate { onRetrievePostListFinish() }
                    .subscribe(
                        { result -> result.body()?.let { onRetrievePostListSuccess(it) } },
                        { onRetrievePostListError(R.string.post_error) }
                    )
            )
        } else {
            onRetrievePostListError(R.string.post_error)
        }
    }

    fun loadNote(){
        if (noteDao.note == null) return
        note.value = noteDao.note
    }

    fun insertNote(noteText: String){
        val note = Note(noteText)
        if (noteDao.note == null) {
            noteDao.insert(note)
        }else{
            noteDao.updateNote(noteDao.note.note, noteText)
        }
    }

    private fun onRetrievePostListStart() {
        loadingVisibility.value = View.VISIBLE
        errorMessage.value = null
    }

    private fun onRetrievePostListFinish() {
        loadingVisibility.value = View.GONE
    }

    private fun onRetrievePostListSuccess(response: ApiResponse) {
        responseData.value = response
    }

    private fun onRetrievePostListError(message: Int) {
        onRetrievePostListFinish()
        errorMessage.value = message
    }
}