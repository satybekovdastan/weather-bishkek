package kg.weather.ui.main

import android.annotation.SuppressLint
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.annotation.StringRes
import android.support.design.widget.Snackbar
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.View
import kg.weather.R
import kg.weather.injection.ViewModelFactory
import kg.weather.ui.main.viewModel.WeatherViewModel
import kotlinx.android.synthetic.main.activity_main.*
import android.widget.EditText
import kg.weather.data.model.ApiResponse


class MainActivity : AppCompatActivity() {

    private lateinit var viewModel: WeatherViewModel
    private var errorSnackbar: Snackbar? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setupViewModel()

    }

    private fun setupViewModel() {
        viewModel = ViewModelProviders.of(this, ViewModelFactory(this)).get(WeatherViewModel::class.java)

        viewModel.errorMessage.observe(this, Observer { errorMessage ->
            if (errorMessage != null) showError(errorMessage) else hideError()
        })

        viewModel.loadingVisibility.observe(this, Observer { isVisible ->
            if (isVisible != null) loadingVisibility(isVisible)
        })

        viewModel.responseData.observe(this, Observer { response ->
            if (response != null) loadData(response)
        })

        viewModel.note.observe(this, Observer { note ->
            loadNote(note!!.note)
        })

    }

    @SuppressLint("SetTextI18n")
    private fun loadData(response: ApiResponse) {
        temp.text = "${response.main.temp_max}°"
        weather.text = response.weather[0].main
        city.text = response.name
        country.text = response.sys.country
        if (response.main.temp_max > 10){
            advice.text = resources.getString(R.string.hot)
        }else{
            advice.text = resources.getString(R.string.cold)
        }
    }

    private fun loadNote(note: String) {
        if (note.isEmpty()) {
            btnNote.text = resources.getString(R.string.add_note)
        }else{
            btnNote.text = resources.getString(R.string.update_note)
        }
        textNote.text = note
    }

    private fun showAlertNote() {
        val view = layoutInflater.inflate(R.layout.alert_edit, null)
        val alertDialog = AlertDialog.Builder(this).create()
        alertDialog.setTitle(R.string.note)
        alertDialog.setCancelable(false)

        val noteEditText = view.findViewById(R.id.etComments) as EditText
        noteEditText.setText(textNote.text)
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, resources.getString(R.string.save)) { dialog, which ->
            val text = noteEditText.text.toString()
            viewModel.insertNote(text)
            loadNote(text)
        }

        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, resources.getString(R.string.cancel)) { dialog, which ->
            alertDialog.dismiss()
        }

        alertDialog.setView(view)
        alertDialog.show()
    }

    fun noteOnClick(view: View) {
        showAlertNote()
    }

    private fun loadingVisibility(visible: Int) {
        progress.visibility = visible
    }

    private fun showError(@StringRes errorMessage: Int) {
        errorSnackbar = Snackbar.make(view, errorMessage, Snackbar.LENGTH_INDEFINITE)
        errorSnackbar?.setAction(R.string.retry, viewModel.errorClickListener)
        errorSnackbar?.show()
    }

    private fun hideError() {
        errorSnackbar?.dismiss()
    }

}
